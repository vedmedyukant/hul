const express     = require('express');
const app         = express();
const mongoClient = require('mongodb').MongoClient;
const assert      = require('assert'); 
const url         = 'mongodb://localhost:27017';
const pug         = require('pug');
const port        = 3000

app.set("view engine", "pug");
app.set("views", "public");
app.use(express.static('public'))

app.get('/', (req, res) =>{
    res.render("OwPages/index")
})
app.get('/about', (req, res) =>{
    res.render("OwPages/about")
})
app.get('/wiki', (req, res) =>{
    res.render("OwPages/wiki")
})
app.get('/main.css', (req, res) =>{
    res.sendFile('css/main.css')
})

app.listen(port)